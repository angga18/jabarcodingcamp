//soal no1 // ouput untuk nilai
// jawaban soal no1
var nilai = 85;

if (nilai >= 85) {
  console.log('Nilai Anda A');
} else if (nilai >= 75 && nilai < 85) {
  console.log('Nilai Anda B');
} else if (nilai >= 65 && nilai < 75) {
  console.log('Nilai Anda C');
} else if (nilai >= 55 && nilai < 65) {
  console.log('Nilai Anda D');
} else {
  console.log('Nilai Anda E');
}

//soal 2 // output untuk bulan
//jawaban soal no 2
var tanggal = 3;
var bulan = 10;
var tahun = 1993;

switch (bulan) {
  case 8: {
    console.log('3');
    break;
  }
  case 9: {
    console.log('oktober');
    break;
  }
  case 10: {
    console.log('3 oktober 1993');
    break;
  }
  default: {
    console.log('Tidak terjadi apa-apa');
  }
}

//soal no 3 // output untuk n = 3 & n =7
//jawaban soal no 3
var s = '';
for (var n = 0; n < 3; n++) {
  for (var j = 0; j <= n; j++) {
    s += '*';
  }
  s += '\n';
}
console.log(s);

//soal no 4 // output untuk m = 3 m = 5 m = 7 m = 10
// jawaban soal no 4
var m =5;
for (i = 1; i <m+1; i++) {
  if (i === 1) {
    console.log('1- i love programing');
  }
  if (i === 2) {
    console.log('2- i love javascript');
  }
  if (i === 3) {
    console.log('3- i love vue js');
  }
  if (i === 4) {
    console.log('4 love programming');
  }
  if (i === 5) {
    console.log('5- i love javascript');
  }
  if (i === 6) {
    console.log('6- i love vue js');
  }
  if (i === 7) {
    console.log('7 love programming');
  }
  if (i === 8) {
    console.log('8- i love javascript');
  }
  if (i === 9) {
    console.log('9- i love vue js');
  }
  if (i === 10) {
    console.log('10- love programming');
  }
}
