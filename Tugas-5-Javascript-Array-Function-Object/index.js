//soal no 1
var daftarHewan = ['2. Komodo', '5. Buaya', '3. Cicak', '4. Ular', '1. Tokek'];

for (var i = 0; i < daftarHewan.length; i++) {
  console.log(daftarHewan.sort()[i]);
}

//soal no 2

function introduce(name, age, address, hobby) {
  var perkenalan = {};
  perkenalan.name = name;
  perkenalan.age = age;
  perkenalan.address = address;
  perkenalan.hobby = hobby;
  return perkenalan;
}

var biodata = introduce('angga', '27', 'cimahi', 'coding');
console.log(
  'nama saya ' + biodata.name,
  'umur saya' + biodata.age,
  'alamat saya di' + biodata.address,
  'dan saya punya hoby' + biodata.hobby
);

//soal no 3

const vowels = ['a', 'e', 'i', 'o', 'u'];

function countVowel(str) {
  var count = 0;

  for (var letter of str.toLowerCase()) {
    if (vowels.includes(letter)) {
      count++;
    }
  }

  return count;
}

var string1 = 'muhamad';
var string2 = 'iqbal';

var hitung_1 = countVowel(string1);
var hitung_2 = countVowel(string2);

console.log(hitung_1, hitung_2);

//soal no 4
function hitung(angka) {
  var hasil1 = angka -2;

  return hasil1;
}

console.log(hitung(0));
