//soal no 1
let luasPersegi = (panjang, lebar) => panjang * lebar;
let kelilingPersegi = (panjang, lebar) => 2 * (panjang + lebar);

let hitungPersegi = (panjang, lebar) => {
  let luas = luasPersegi(panjang, lebar);
  let keliling = kelilingPersegi(panjang, lebar);
  return [luas, keliling];
};

let [luas, keliling] = hitungPersegi(5, 4);
console.log('luas persegi adalah', luas);
console.log('keliling persegi adalah', keliling);

//soal no2
const newFunction = (firstName, lastName) => {
  return {
    firstName,
    lastName,
    fullname: () => {
      console.log(firstName + ' ' + lastName);
    },
  };
};
//Driver Code
var biodata = newFunction('wiliam', 'imoh').fullname();

//soal no 3
const newObject = {
  firstName: 'Muhammad',
  lastName: 'Iqbal Mubarok',
  address: 'Jalan Ranamanyar',
  hobby: 'playing football',
};
const { firstName, lastName, address, hobby } = newObject;
console.log(firstName, lastName, address, hobby);

//soal no 4
const west = ['Will', 'Chris', 'Sam', 'Holly'];
const east = ['Gill', 'Brian', 'Noel', 'Maggie'];

const combid = [...west, ...east];
console.log(combid);

// soal no 5
const planet = 'earth';
const view = 'glass';

var before =
  'Lorem ' +
  `${view},` +
  'dolor sit amet, ' +
  'consectetur adipiscing elit,' +
  `${planet}`;
console.log(before);
